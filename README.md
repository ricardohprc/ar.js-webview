# AR Webview

Usar o QR Code para ativar o software: [qr marker](https://bitbucket.org/ricardohprc/ar.js-webview/src/master/images/marker.png)

## Como Rodar

1 - Instalar Node.js e NPM
2 - Na pasta do Projeto
``` npm install ```
``` npm start ```

## Built With

* [ThreeJS](http://threejs.org) - For the 3D
* [ARJS](https://github.com/jeromeetienne/AR.js/) - For the Web AR
* [SweetAlert2](https://sweetalert2.github.io/) - For the cool popup messages
* [Youtube API](https://developers.google.com/youtube/v3/) - For the webcam live streaming
